#include <ctime>
#include <filesystem>
#include <inotifytools/inotify.h>
#include <inotifytools/inotifytools.h>
#include <iostream>
#include <limits.h>
#include <pwd.h>
#include <stdlib.h>
#include <string>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "main.hpp"

int HandleEditing::getUid() {
    char *username = getenv("USER");
    struct passwd *pwd = getpwnam(username);

    return pwd->pw_uid;
}

int HandleEditing::getGid() {
    char *username = getenv("USER");
    struct passwd *pwd = getpwnam(username);

    return pwd->pw_gid;
}

bool HandleEditing::runEditor(std::string editorProg, std::string tmpFilePath) {
    const char *charEditorProg = editorProg.c_str();
    const char *charTmpFilePath = tmpFilePath.c_str();
    childPid = fork();

    switch(childPid) {
        case -1:
            std::cerr << "Forking failed!";
            return false;
        case 0:
            if (setuid(getUid()) == -1 ) {
                std::cerr << "Failed to set UID for editor!";
                return false;
            }
            execlp(charEditorProg, charEditorProg, charTmpFilePath, NULL);
            exit( EXIT_FAILURE );
            break;
        default:
            break;
    }

    wait(&status);
    return true;
}

bool SetupInotify::setUpInotify(char *filePath) {
    if (!inotifytools_initialize()) {
        return false;
    } else if(!inotifytools_watch_file(filePath, IN_MODIFY)) {
            return false;
    } else {
        return true;
    }
}

std::string SetupInotify::copyFile(const std::string origFilePath) {
    const std::string tmpDir = std::filesystem::temp_directory_path();
    const std::string timeSinceEpoch = std::to_string(std::time(nullptr));
    const std::string tmpFile = tmpDir + "/cppedit-" + timeSinceEpoch;

    std::filesystem::copy(origFilePath, tmpFile);

    return tmpFile;
}

bool SetupInotify::chownFile(std::string tmpFile) {
    HandleEditing editObj;
    if (chown(tmpFile.c_str(), editObj.getUid(), editObj.getGid()) == -1) {
        return false;
    }
    return true;
}

bool SyncFile::wasModified() {
    struct inotify_event * event = inotifytools_next_event(0);

    if (event) {
        return true;
    }

    return false;
}

void SyncFile::overwriteOrigFile(std::string tempFilePath, std::string origFilePath) {
    std::filesystem::copy(tempFilePath, origFilePath, std::filesystem::copy_options::overwrite_existing);
}

//TODO: Error handling for user input
int main(int argc, char *argv[]) {
    SetupInotify inotifyObj;
    HandleEditing editObj;
    SyncFile syncObj;

    if (argc < 2) {
        std::cerr << "Please supply at least one filename!";
        return 1;
    }

    std::string filePath = inotifyObj.copyFile(argv[1]);

    if(!inotifyObj.chownFile(filePath)) {
        std::cerr << "Couldn't chown the tmpfile to make it writeable to user!";
        return 1;
    }

    std::string editorProg = argv[2];

    if(!inotifyObj.setUpInotify(&filePath[0u])) {
        std::cerr << "Couldn't watch file with inotify";
        return 1;
    }

    if(!editObj.runEditor(editorProg, filePath)) {
        std::cerr << "Couldn't start editor program.";
        return 1;
    }

    if (syncObj.wasModified()) {
        syncObj.overwriteOrigFile(filePath, argv[1]);
    }

    return 0;
}
