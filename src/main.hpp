#ifndef UEDIT_H
#define UEDIT_H

class HandleEditing {
    private:
        std::string progRun;
        int status;
        pid_t childPid;
    public:
        int getUid();
        int getGid();
        bool runEditor(std::string editorProg, std::string editTmpFile);
};

class SetupInotify : public HandleEditing {
    public:
        bool setUpInotify(char *filePath);
        std::string copyFile(const std::string origFilePath);
        bool chownFile(std::string tmpFilePath);
};

class SyncFile : public SetupInotify {
    public:
        bool wasModified();
        void overwriteOrigFile(std::string tempFilePath, std::string origFilePath);
};

#endif